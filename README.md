### mlb 2021/11/10 ###

To run this environnement you need Docker.

Projects are in www folder :
- The back-end project use Symfony 4.4.33 which is the actual LTS release (10/11/2021).
- The front-end project use Angular 13.

In case of problems
- you might use an Ubuntu 20.04LTS system;
- you must generate a keypair for lexik, command is php bin/console lexik:jwt:generate-keypair
