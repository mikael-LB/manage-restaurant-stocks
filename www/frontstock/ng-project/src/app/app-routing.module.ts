import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './forms/connexion/connexion.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfilComponent } from './pages/users/profil/profil.component';
import { ProductsListComponent } from './pages/products/products-list/products-list.component';

const routes: Routes = [
  { path: '', component: ConnexionComponent },
  { path: 'accueil', component: HomepageComponent,/* canActivate:[AuthGuard] */},
  { path: 'profil', component: ProfilComponent,/* canActivate:[AuthGuard] */},
  { path: 'consulter-stock', component: ProductsListComponent,/* canActivate:[AuthGuard] */},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
