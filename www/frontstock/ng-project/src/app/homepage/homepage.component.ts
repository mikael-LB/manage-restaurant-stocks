import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { User } from '../shared/models/user';
import { UserToken } from '../shared/models/user-token';
import { UserWebserviceService } from '../shared/webservices/user-webservice.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  //declare a user and a subscription to userSubject
  //useful to know if user is admin
  user!:User;
  userSubscription$!: Subscription;
  //login initial state is false
  isLoggedIn = false;
  //isAdmin
  isAdmin:boolean = false;

  constructor(
    private userWS: UserWebserviceService,
    private router: Router,
    private authService: AuthService) {
      this.isLoggedIn = authService.isLoggedIn;
     }

  ngOnInit(): void {
    this.userSubscription$ = this.userWS.getUserSubject().subscribe(
      {
        next: (data: User) => {
          this.user = data;
          let isTrue = this.user.roles?.includes("ROLE_ADMIN");
          isTrue === true ? this.isAdmin = true: this.isAdmin = false;
        }
      }
    )
  }

}
