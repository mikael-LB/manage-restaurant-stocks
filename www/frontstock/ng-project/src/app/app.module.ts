import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToolbarComponent } from './shared/components/toolbar/toolbar.component';
import { ConnexionComponent } from './forms/connexion/connexion.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from './shared/components/nav/navbar/navbar.component';
import { FooterComponent } from './shared/components/footer/footer/footer.component';
import { ProfilComponent } from './pages/users/profil/profil.component';
import { HeaderComponent } from './shared/components/header/header/header.component';
import { ProductsListComponent } from './pages/products/products-list/products-list.component';
import { ProductItemComponent } from './pages/products/product-item/product-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ConnexionComponent,
    HomepageComponent,
    NavbarComponent,
    FooterComponent,
    ProfilComponent,
    HeaderComponent,
    ProductsListComponent,
    ProductItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
