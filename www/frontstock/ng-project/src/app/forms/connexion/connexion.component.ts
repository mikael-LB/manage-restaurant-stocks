import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { UserToken } from 'src/app/shared/models/user-token';
import { ProductWebserviceService } from 'src/app/shared/webservices/product-webservice.service';
import { UserWebserviceService } from 'src/app/shared/webservices/user-webservice.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  connexionForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  //userToken: UserToken = new UserToken();
  //userTokenSubscription$!: Subscription;

  //errorMessage
  errorMessage!:string;
  errorMessageSubscription$!: Subscription;

  constructor(
    private userWS: UserWebserviceService,
    private productWS: ProductWebserviceService,
    private authService:AuthService,
    private router: Router) {}

  ngOnInit(): void {
    // this.userTokenSubscription$ = this.userWS.getTokenSubject().subscribe(
    //   (data: UserToken) => {
    //     this.userToken = data;
    //   }
    // );
    this.errorMessageSubscription$ = this.productWS.getErrorMessageSubject().subscribe(
      (data:string)=>{
        this.errorMessage = data;
        console.log(data);
      }
    )
  }

  ngOnDestroy(): void {
    // if (this.userTokenSubscription$) {
    //   this.userTokenSubscription$.unsubscribe();
    // }
    if (this.errorMessageSubscription$) {
      this.errorMessageSubscription$.unsubscribe();
    }
  }

  onSubmit() {
    const formValue = this.connexionForm.value;
    //console.log(formValue);
    this.userWS.getToken(formValue).subscribe(
      {
        next: (data: UserToken) => {
          /* Subject is already update with value in the userWS.getToken method
          this.userToken = data;
          this.userWS.setTokenSubject(this.userToken);
          */
          //console.log("data component TOKEN \n" + this.userToken.token);
          this.userWS.retrieveThisUser().subscribe(
            (data)=>{
              //user is retrieved so auth is ok so redirect to /accueil
              this.authService.login().subscribe(()=>{
                if (this.authService.isLoggedIn){
                  this.router.navigate(['/accueil']);
                }
              }
              );
              //console.log("retrieveuser");
            }
          );
        },
        error: (err) => {
          //keep error if case of need
          //console.log("error is \n" + err); //print undefined
          /*
          if (err instanceof HttpErrorResponse){
            if (err.status === 401){
              // console.log for err.status doesn't work here, normal thing,
              // but ok in service
              console.log("401 from component");
            }
          }*/
          this.errorMessage = "Identifiants invalides";
        }
      }
    );
  }

}
