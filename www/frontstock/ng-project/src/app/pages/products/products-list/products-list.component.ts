import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/shared/models/product';
import { ProductWebserviceService } from 'src/app/shared/webservices/product-webservice.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  productList!:Product[];

  constructor(
    private productWS:ProductWebserviceService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.productWS.retrieveAllProducts().subscribe(
      {
        next: (data) => {
          this.productList = data;
        },
        error: (err) => {
          this.router.navigate(['/']);
        }
      }
    );
  }

}
