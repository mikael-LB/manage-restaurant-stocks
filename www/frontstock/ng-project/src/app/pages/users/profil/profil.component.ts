import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/shared/models/user';
import { UserToken } from 'src/app/shared/models/user-token';
import { UserWebserviceService } from 'src/app/shared/webservices/user-webservice.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  user!:User;
  userSubscription$!:Subscription;
//declare a token and a subscription to userTokenSubject
userToken!:UserToken;
userTokenSubscription$!: Subscription;
//login initial state is false
isLoggedIn = false;

constructor(
  private userWS: UserWebserviceService,
  private router: Router,
  private authService: AuthService
  ) {
  this.isLoggedIn = authService.isLoggedIn;
  
}

  ngOnInit(): void {
    this.userSubscription$ = this.userWS.getUserSubject().subscribe(
      {
        next: (data: User) => {
          this.user = data;
        }
      }
    )
    this.userTokenSubscription$ = this.userWS.getTokenSubject().subscribe(
      {
        next: (data: UserToken) => {
          this.userToken = data;
        }
      }
    )
  }

}
