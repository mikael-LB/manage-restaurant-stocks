import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, catchError, map, Observable, Subject, Subscription, throwError } from 'rxjs';
import { UserToken } from '../models/user-token';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserWebserviceService {
  baseUrl = "http://applistock.localhost/api";

  protected requestHeaders = {
    headers: new HttpHeaders()
      .set('content-type', 'application/json')
  };

  //tokenSubject for userToken
  userToken = new UserToken();
  private tokenSubject$ = new BehaviorSubject<UserToken>(this.userToken);

  //userSubject for user
  user = new User();
  private userSubject$ = new BehaviorSubject<User>(this.user);

  constructor(private http: HttpClient) { }

  /**
   * Method to set the Token Subject
   * @param userToken:UserToken 
   */
  setTokenSubject(userToken: UserToken) {
    this.tokenSubject$.next(userToken);
  }

  /**
   * Method to get the Token Subject
   * @returns Observable<UserToken>
   */
  getTokenSubject(): Observable<UserToken> {
    return this.tokenSubject$.asObservable();
  }

  /**
   * Method to set the User Subject
   * @param userToken:UserToken 
   */
  setUserSubject(user: User) {
    console.log(user);
    this.userSubject$.next(user);
  }

  /**
   * Method to get the User Subject
   * @returns Observable<UserToken>
   */
  getUserSubject(): Observable<User> {
    return this.userSubject$.asObservable();
  }

  /**
   * Method to get the Token
   * @param formValue with email and password
   * @returns Observable<UserToken>
   */
  getToken(formValue: any): Observable<UserToken> {
    //in form, field is named "email" but in db it's "username"
    let data = { "username": formValue.email, "password": formValue.password };
    return this.http.post(this.baseUrl + "/login_check", JSON.stringify(data), this.requestHeaders).pipe(
      map((res) => {
        if (res.hasOwnProperty('token')) {
          //console.log("data has property token");
          this.userToken = res;
          this.setTokenSubject(this.userToken);
          return res;
        }
        throw new Error("data doesn't have property token");
      }),
      catchError((err) => {
        this.treatError(err);
        return throwError(() => { console.log("error thrown in service") }); //or new Error()
      })
    );
    /*syntax can't treat error because deprecated, use .pipe see above
    .subscribe(
      (data)=>{console.log("data du service" + data); this.userToken = data; this.emitTokenSubject();}
    );*/
  }

  /**
   * Method to retrieve the user with token value
   * @returns Observable<User>
   */
  retrieveThisUser(): Observable<User> {
    /*after a few try i found that append doesn't work, but why?
    So i have to create a new complete HttpHeaders
    this.requestHeaders.append(
       'Authorization',bearer
    );*/
    let requestHeaders = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Authorization', 'Bearer ' + this.userToken.token);
    //console.log(requestHeaders);

    return this.http.get(this.baseUrl + "/users/user", { 'headers': requestHeaders }).pipe(
      map((res) => {
        if (res.hasOwnProperty('email')) {
          //console.log("res (user) has property email");
          this.user = res;
          this.setUserSubject(this.user);
          return res;
        }
        throw new Error("data doesn't have property email");
      }),
      catchError((err) => {
        this.treatError(err);
        return throwError(() => { console.log("error throw in service") });
      })
    );
  }

  /**
   * Method to treat the error from the http response
   * @param err 
   */
  private treatError(err:Error) {
    //console.log("Erreur service");
    if (err instanceof HttpErrorResponse) {
      if (err.status === 401) {
        // TODO redirect to connexion page
        console.log("401 from service : " + err.message);
      }
    }
  }
}
