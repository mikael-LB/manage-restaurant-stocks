import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, Observable, Subject, Subscription, throwError } from 'rxjs';
import { Product } from '../models/product';
import { UserToken } from '../models/user-token';
import { UserWebserviceService } from './user-webservice.service';

@Injectable({
  providedIn: 'root'
})
export class ProductWebserviceService {
  baseUrl = "http://applistock.localhost/api";

  protected requestHeaders = {
    headers: new HttpHeaders()
      .set('content-type', 'application/json')
  };

  //userToken Observer
  userToken = new UserToken();
  private userTokenSubscription$! : Subscription;

  //productListSubject
  productList: any[];
  productListSubject$ = new Subject<Product[]>();

  //errorMessageSubject
  errorMessage!:string;
  errorMessageSubject$ = new BehaviorSubject<string>(this.errorMessage);

  constructor(
    private http:HttpClient,
    private userWS:UserWebserviceService,
    ) { 
    this.productList = [];
  }

  /**
   * Method to set the productListSubject$
   */
  public setProductListSubject(productList:[]){
    this.productListSubject$.next(productList);
  }

  /**
   * Method to get the productListSubject$
   * @return Observable<Product[]>
   */
  public getProductListSubject():Observable<Product[]>{
    return this.productListSubject$.asObservable();
  }

  /**
   * Method to set the errorMessageSubject
   */
  public setErrorMessageSubject(message:string){
    this.errorMessageSubject$.next(message);
  }

  /**
   * Method to get the errorMessageSubject
   * @returns Observable<string>
   */
   public getErrorMessageSubject(){
    return this.errorMessageSubject$.asObservable();
  }

  /**
   * Method to retrieve all products
   * return Observable<Product[]>
   */
  public retrieveAllProducts():Observable<Product[]>
  {
    this.initUserTokenSubscription();

    let requestHeaders = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Authorization', 'Bearer ' + this.userToken.token);
    
      return this.http.get<Product[]>(this.baseUrl + "/products", { 'headers': requestHeaders }).pipe(
        map((res) => {
          // here, with the <> after the http.get, res is already a Product[]
          // because object attributes are the same in API and in Product
          // but in case of need we can change something below
          /* console.log(res);
          for (let item of res){          
            let product:Product = item;          
            console.log("id item "+ item.designation);
            this.productList.push(product);
          }
          return this.productList;*/
          return res;
        }),
        catchError((err) => {
          this.treatError(err);
          return throwError(() => { console.log("error throw in service") });
        })
      );
  }

   /**
   * Method to treat the error from the http response
   * @param err 
   */
    private treatError(err:Error) {
      //console.log("Erreur service");
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // TODO redirect to connexion page
          console.log("401 from service : " + err.message);
          this.setErrorMessageSubject("Authentification requise");
        }
      }
    }

    /**
     * Method to set the userTokenSubscription
     */
    private initUserTokenSubscription(): void {
      this.userTokenSubscription$ = this.userWS.getTokenSubject().subscribe(
        {
          next: (data:UserToken) => {
            this.userToken = data;
          }
        }
      );
    }
}
