import { TestBed } from '@angular/core/testing';

import { ProductWebserviceService } from './product-webservice.service';

describe('ProductWebserviceService', () => {
  let service: ProductWebserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductWebserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
