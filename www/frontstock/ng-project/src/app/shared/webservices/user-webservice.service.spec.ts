import { TestBed } from '@angular/core/testing';

import { UserWebserviceService } from './user-webservice.service';

describe('UserWebserviceService', () => {
  let service: UserWebserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserWebserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
