import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/shared/models/user';
import { UserToken } from 'src/app/shared/models/user-token';
import { UserWebserviceService } from 'src/app/shared/webservices/user-webservice.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  //declare a user and a subscription to userSubject
  user!:User;
  userSubscription$!: Subscription;
  //declare a token and a subscription to userTokenSubject
  userToken!:UserToken;
  userTokenSubscription$!: Subscription;
  //login initial state is false
  isLoggedIn = false;

  constructor(
    private userWS: UserWebserviceService,
    private router: Router,
    private authService: AuthService
    ) {
    this.isLoggedIn = authService.isLoggedIn;
    
  }

  ngOnInit(): void {
    this.userSubscription$ = this.userWS.getUserSubject().subscribe(
      {
        next: (data: User) => {
          this.user = data;
        }
      }
    )
    this.userTokenSubscription$ = this.userWS.getTokenSubject().subscribe(
      {
        next: (data: UserToken) => {
          this.userToken = data;
        }
      }
    )
  }

  ngOnDestroy(): void {
    if (this.userSubscription$) {
      this.userSubscription$.unsubscribe();
    }
    if (this.userTokenSubscription$) {
      this.userTokenSubscription$.unsubscribe();
    }
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/']);
    this.user = new User();
    this.userWS.setUserSubject(this.user);
    this.userToken = new UserToken();
    this.userWS.setTokenSubject(this.userToken);
  }

}
