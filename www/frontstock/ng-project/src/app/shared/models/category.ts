export class Category {
    public id?:number;
    public designation?:string;

    constructor(
        id?:number,
        designation?:string
    ){
        this.id = id;
        this.designation = designation;
    }
}
