export class Unity {
    public id?:number;
    public designation?:string;
    public symbol?:string;

    constructor(
        id?:number,
        designation?:string,
        symbol?:string,
    ){
        this.id = id;
        this.designation = designation;
        this.symbol = symbol;
    }
}
