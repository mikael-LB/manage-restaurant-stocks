import { JobTitle } from "./job-title";

export class User {
    public id?:number;
    public email?:string;
    public roles?:string[];
    //public password?:string;
    public lastname?:string;
    public firstname?:string;
    public photoUrl?:string;
    public isActivated?:boolean;
    public jobtitle?:JobTitle;

    constructor(
        id?:number,
        email?:string,
        roles?:string[],
        //password?:string,
        lastname?:string,
        firstname?:string,
        photoUrl?:string,
        isActivated?:boolean,
        jobtitle?:JobTitle
        ){
            this.id = id;
            this.email = email;
            this.roles = roles;
            //this.password = password;
            this.lastname = lastname;
            this.firstname = firstname;
            this.photoUrl = photoUrl;
            this.isActivated = isActivated;
            this.jobtitle = jobtitle;
        }
}
