export class Supplier {
    public id?:number;
    public name?:string;
    public phone?:string;
    public activity?:string;

    constructor(
        id?:number,
        name?:string,
        phone?:string,
        activity?:string
    ){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.activity = activity;
    }
}
