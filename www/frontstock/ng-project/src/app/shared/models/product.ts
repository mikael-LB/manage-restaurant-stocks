import { Category } from "./category";
import { Supplier } from "./supplier";
import { Unity } from "./unity";

export class Product {
    public id?:number;
    public designation?:string;
    public qtyInStck?:number;
    public price?:number;
    public category?:Category;
    public supplier?:Supplier;
    public unity?:Unity;
    //public composes?:

    constructor(
        id?:number,
        designation?:string,
        qtyInStck?:number,
        price?:number,
        category?:Category,
        supplier?:Supplier,
        unity?:Unity,
        //composes?:
    ){
        this.id = id;
        this.designation = designation;
        this.qtyInStck = qtyInStck;
        this.price = price;
        this.category = category;
        this.supplier = supplier;
        this.unity = unity;
        //this.composes = composes;
    }
}
