<?php

namespace App\Service;

use App\Entity\Supplier;
use App\Repository\SupplierRepository;

class SupplierService
{
    protected SupplierRepository $supplierRepository;

    public function __construct(
        SupplierRepository $supplierRepository
    ){
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * Method to get a Supplier for a specific id
     * @return Supplier
     */
    public final function getSupplierById(int $id): Supplier
    {
        return $this->supplierRepository->findOneById($id);
    }

    /**
     * Method to get a Supplier for a specific name
     * @return Supplier
     */
    public final function getSupplierByName(string $name): Supplier
    {
        return $this->supplierRepository->findOneBy(array("name" => $name));
    }
}