<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;

class ProductService
{
    protected ProductRepository $productRepository;

    public function __construct(
        ProductRepository $productRepository
    ){
        $this->productRepository = $productRepository;
    }

    /**
     * Method to get a Product for a specific id
     * @return Product
     */
    public final function getProductById(int $id): Product
    {
        return $this->productRepository->findOneById($id);
    }

    /**
     * Method to get a Product for a specific designation
     * @return Product
     */
    public final function getProductByDesignation(string $designation): Product
    {
        return $this->productRepository->findOneBy(array("designation" => $designation));
    }

    /**
     * Metod to retrieve ids for Category, Supplier, Unity from the json
     */
    public final function retrieveIds(string $json):array
    {
        $ids = [];
        $ids["category"] = json_decode($json)->category->id;
        $ids["supplier"] = json_decode($json)->supplier->id;
        $ids["unity"] = json_decode($json)->unity->id;
        return $ids;
    }
}