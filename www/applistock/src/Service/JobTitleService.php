<?php

namespace App\Service;

use App\Entity\JobTitle;
use App\Repository\JobTitleRepository;

class JobTitleService
{
    protected JobTitleRepository $jobtitleRepository;

    public function __construct(
        JobTitleRepository $jobtitleRepository
    ){
        $this->jobtitleRepository = $jobtitleRepository;
    }

    /**
     * Method to get a JobTitle for a specific id
     * @return JobTitle
     */
    public final function getJobTitleById(int $id): JobTitle
    {
        return $this->jobtitleRepository->findOneById($id);
    }
}