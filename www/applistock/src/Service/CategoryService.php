<?php

namespace App\Service;

use App\Entity\Category;
use App\Repository\CategoryRepository;

class CategoryService
{
    protected CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository
    ){
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Method to get a Category for a specific id
     * @return Category
     */
    public final function getCategoryById(int $id): Category
    {
        return $this->categoryRepository->findOneById($id);
    }

    /**
     * Method to get a Category for a specific designation
     * @return Category
     */
    public final function getCategoryByDesignation(string $designation): Category
    {
        return $this->categoryRepository->findOneBy(array("designation" => $designation));
    }
}