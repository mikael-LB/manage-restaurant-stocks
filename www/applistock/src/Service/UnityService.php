<?php

namespace App\Service;

use App\Entity\Unity;
use App\Repository\UnityRepository;

class UnityService
{
    protected UnityRepository $unityRepository;

    public function __construct(
        UnityRepository $unityRepository
    ){
        $this->unityRepository = $unityRepository;
    }

    /**
     * Method to get a Unity for a specific id
     * @return Unity
     */
    public final function getUnityById(int $id): Unity
    {
        return $this->unityRepository->findOneById($id);
    }

    /**
     * Method to get a Unity for a specific symbol
     * @return Unity
     */
    public final function getUnityBySymbol(string $symbol): Unity
    {
        return $this->unityRepository->findOneBy(array("symbol" => $symbol));
    }
}