<?php

namespace App\Service;

use App\Entity\Category;
use App\Repository\CategoryRepository;

class EntityServiceFactory
{
    protected ProductService $productService;
    protected CategoryService $categoryService;
    protected SupplierService $supplierService;
    protected UnityService $unityService;
    protected JobTitleService $jobtitleService;

    public function __construct(
        ProductService $productService,
        CategoryService $categoryService,
        SupplierService $supplierService,
        UnityService $unityService,
        JobTitleService $jobtitleService
    ){
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->supplierService = $supplierService;
        $this->unityService = $unityService;
        $this->jobtitleService = $jobtitleService;
    }

    /**
     * Method to get the ProductService
     * @return ProductService
     */
    public final function getProductService(): ProductService
    {
        return $this->productService;
    }

    /**
     * Method to get the CategoryService
     * @return CategoryService
     */
    public final function getCategoryService(): CategoryService
    {
        return $this->categoryService;
    }

    /**
     * Method to get the SupplierService
     * @return SupplierService
     */
    public final function getSupplierService(): SupplierService
    {
        return $this->supplierService;
    }

    /**
     * Method to get the UnityService
     * @return UnityService
     */
    public final function getUnityService(): UnityService
    {
        return $this->unityService;
    }

    /**
     * Method to get the JobTitleService
     * @return JobTitleService
     */
    public final function getJobTitleService(): JobTitleService
    {
        return $this->jobtitleService;
    }
}