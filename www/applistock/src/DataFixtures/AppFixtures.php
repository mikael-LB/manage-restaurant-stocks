<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Compose;
use App\Entity\Inventory;
use App\Entity\JobTitle;
use App\Entity\Plate;
use App\Entity\Product;
use App\Entity\Quantity;
use App\Entity\Supplier;
use App\Entity\Type;
use App\Entity\Unity;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\JobTitleRepository;
use App\Repository\PlateRepository;
use App\Repository\ProductRepository;
use App\Repository\SupplierRepository;
use App\Repository\UnityRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class to create a set of data in the database
 * which provide the ability to test the API features
 */
class AppFixtures extends Fixture
{
    protected UserRepository $userRepository;
    protected JobTitleRepository $jobTitleRepository;
    protected CategoryRepository $categoryRepository;
    protected SupplierRepository $supplierRepository;
    protected UnityRepository $unityRepository;
    protected PlateRepository $plateRepository;
    protected ProductRepository $productRepository;
    protected $passwordEncoder;

    public function __construct(
        UserRepository $userRepository,
        JobTitleRepository $jobTitleRepository,
        CategoryRepository $categoryRepository,
        SupplierRepository $supplierRepository,
        UnityRepository $unityRepository,
        PlateRepository $plateRepository,
        ProductRepository $productRepository,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->userRepository = $userRepository;
        $this->jobTitleRepository = $jobTitleRepository;
        $this->categoryRepository = $categoryRepository;
        $this->supplierRepository = $supplierRepository;
        $this->unityRepository = $unityRepository;
        $this->plateRepository = $plateRepository;
        $this->productRepository = $productRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Method to add some fake data in the database
     * very useful for the developpment
     */
    public function load(ObjectManager $manager): void
    {
        // Add an Inventory
        $inventory = new Inventory();
        $inventory->setDateInventory(new DateTime('2021-04-30 16:00:00'))
            ->setIsValidated(true);
        $manager->persist($inventory);
        $manager->flush();

        // Add some Type
        $lstType = ["Plat", "Produit"];
        foreach ($lstType as $value) {
            $type = new Type();
            $type->setTypename($value);
            $manager->persist($type);
        }
        $manager->flush();

        // Add some Unity
        $lstUnity = [
            "bidon" => "Bidon",
            "bte" => "Boite",
            "bouteille" => "Bouteille",
            "douzaine" => "Douzaine",
            "kg" => "Kilogramme",
            "L" => "Litre",
            "pce" => "Pièce",
        ];
        foreach ($lstUnity as $key => $value) {
            $unity = new Unity();
            $unity->setDesignation($value)
                ->setSymbol($key);
            $manager->persist($unity);
        }
        $manager->flush();

        // Add some Supplier
        $supplier1 = new Supplier();
        $supplier1->setName("SupersProduits-F1")
            ->setPhone("0601020304")
            ->setActivity("Vente de produits");
        $manager->persist($supplier1);
        //
        $supplier2 = new Supplier();
        $supplier2->setName("LaBonneGlace-F2")
            ->setPhone("0602030405")
            ->setActivity("Fabricant de Glaces");
        $manager->persist($supplier2);
        //
        $supplier3 = new Supplier();
        $supplier3->setName("CotEtGroin-F3")
            ->setPhone("0603040506")
            ->setActivity("Eleveur de volailles et porcs");
        $manager->persist($supplier3);
        //
        $supplier4 = new Supplier();
        $supplier4->setName("SansSommeil-F4")
            ->setPhone("+33604050607")
            ->setActivity("Meunier");
        $manager->persist($supplier4);
        $manager->flush();

        // Add some Categories
        $lstCategory = [
            "Gale" => "Galettes",
            "Crep" => "Crêpes",
            "Cidr" => "Cidre",
            "Char" => "Charcuterie",
            "Glac" => "Glaces",
            "Legu" => "Légumes",
            "Fari" => "Farine",
            "Oeuf" => "Oeufs",
            "Vian" => "Viandes",
            "PrLa" => "Produits laitiers",
            "Autr" => "Autre"
        ];
        foreach ($lstCategory as $value) {
            $category = new Category();
            $category->setDesignation($value);
            $manager->persist($category);
        }
        $manager->flush();

        // Add some JobTitle
        $lstJobTitles = ["Crêpier.ère", "Gérant.e", "Serveur.euse"];

        foreach ($lstJobTitles as $value) {
            $jobTitle = new JobTitle();
            $jobTitle->setName($value);
            $manager->persist($jobTitle);
        }
        $manager->flush();

        // Add some Users, one ADMIN and one USER
        $simpleUser = new User();
        $simpleUser->setEmail('s@mail.com')
            ->setRoles(["ROLE_USER"])
            ->setPassword($this->passwordEncoder->encodePassword($simpleUser, 'password'))
            ->setFirstname("Jean")
            ->setLastname("Fourne")
            ->setIsActivated(true)
            ->setJobtitle($this->jobTitleRepository->findOneBy(array('name' => "Crêpier.ère")));
        $manager->persist($simpleUser);
        $adminUser = new User();
        $adminUser->setEmail('a@mail.com')
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->passwordEncoder->encodePassword($adminUser, 'password'))
            ->setFirstname("Jacques")
            ->setLastname("Celère")
            ->setIsActivated(true)
            ->setJobtitle($this->jobTitleRepository->findOneBy(array('name' => "Gérant.e")));
        $manager->persist($adminUser);
        $manager->flush();

        // Add some Products
        // [Designation, Quantity in stock, Price per unit, 
        // Category name, Supplier name, Unity symbol,
        // Quantity for inventory]
        $lstProduct = array(
            //Fournisseur1
            ["Lait entier", 20, 0.50, $lstCategory["PrLa"], "Fournisseur1", "L", 17],
            ["Emmental", 1.20, 0.30, $lstCategory["PrLa"], "Fournisseur1", "kg", 0.520],
            ["Chocolat noir 5kg", 2, 40.00, $lstCategory["Autr"], "Fournisseur1", "kg", 1.250],
            ["Pansement", 2, 4.99, $lstCategory["Autr"], "Fournisseur1", "bte", 2],
            ["Cidre brut", 10, 2.49, $lstCategory["Cidr"], "Fournisseur1", "bouteille", 12],
            ["Salade mélange", 0.500, 2.49, $lstCategory["Legu"], "Fournisseur1", "kg", 0.750],
            ["Champignon", 0.500, 4.49, $lstCategory["Legu"], "Fournisseur1", "kg", 0.650],
            //Fournisseur2
            ["Glace vanille", 2, 5.45, $lstCategory["Glac"], "Fournisseur2", "L", 1.5],
            ["Glace chocolat", 3.5, 5.45, $lstCategory["Glac"], "Fournisseur2", "L", 1.5],
            ["Glace thé vert", 5, 5.45, $lstCategory["Glac"], "Fournisseur2", "L", 2.25],
            ["Glace romarin", 1.5, 5.45, $lstCategory["Glac"], "Fournisseur2", "L", 0.25],
            //Fournisseur3
            ["Oeuf", 10, 0.30, $lstCategory["Oeuf"], "Fournisseur3", "douzaine", 8],
            ["Filet de poulet", 2, 8.90, $lstCategory["Vian"], "Fournisseur3", "kg", 1.250],
            ["Saucisse grosse", 2.6, 6.70, $lstCategory["Char"], "Fournisseur3", "kg", 0.8],
            ["Jambon tranche", 50, 0.30, $lstCategory["Oeuf"], "Fournisseur3", "pce", 48],
            //Fournisseur4
            ["Farine de blé (kg)", 15, 1.99, $lstCategory["Fari"], "Fournisseur4", "kg", 11.5],
            ["Farine de blé noir", 12.5, 2.25, $lstCategory["Fari"], "Fournisseur4", "kg", 10.5],
        );

        foreach ($lstProduct as $value) {
            $product = new Product();
            $product->setDesignation($value[0])
                ->setQtyInStck($value[1])
                ->setPrice($value[2])
                ->setCategory($this->getCategoryByDesignation($value[3]))
                ->setSupplier($this->getSupplierByName($value[4]))
                ->setUnity($this->getUnityBySymbol($value[5]));
            $manager->persist($product);
            // link product with inventory
            $inventoryQuantity = new Quantity();
            $inventoryQuantity->setTotalPerUnit($value[6])
                ->setOfProduct($product)
                ->setForInventory($inventory);
            $manager->persist($inventoryQuantity);
        }
        $manager->flush();

        // Add some Plates
        $lstPlate = [
            "Galette complète avec glace", "Galette champignons",
            "Crêpe avec dés de filet de poulet", "Crêpe au chocolat"
        ];
        foreach ($lstPlate as $value) {
            $plate = new Plate();
            $plate->setName($value);
            //contains Galette
            if (str_contains($value, 'Galette')) {
                $plate->setCategory($this->getCategoryByDesignation("Galettes"));
            }
            //start with Cr
            if (str_starts_with($value, "Cr")) {
                $plate->setCategory($this->getCategoryByDesignation("Crêpes"));
            }
            $manager->persist($plate);
        }
        $manager->flush();

        //link product with plate
        $lstPlatesInDB = $this->plateRepository->findAll();
        $lstProductsInDB = $this->productRepository->findAll();
        $lstCompose = [
            //Galette complète avec glace
            [$lstPlatesInDB[0], $lstProductsInDB[0], 0.1],//lait
            [$lstPlatesInDB[0], $lstProductsInDB[10], 3],//oeuf
            [$lstPlatesInDB[0], $lstProductsInDB[15], 0.1],//farine de blé noir
            [$lstPlatesInDB[0], $lstProductsInDB[16], 0.05],//champignon
            [$lstPlatesInDB[0], $lstProductsInDB[14], 0.05],//jambon
            [$lstPlatesInDB[0], $lstProductsInDB[5], 0.05],//salade
            [$lstPlatesInDB[0], $lstProductsInDB[10], 0.75],//glace romarin
            //Galette champignons
            [$lstPlatesInDB[1], $lstProductsInDB[0], 0.1],//lait
            [$lstPlatesInDB[1], $lstProductsInDB[10], 2],//oeuf
            [$lstPlatesInDB[1], $lstProductsInDB[15], 0.1],//farine de blé noir
            [$lstPlatesInDB[1], $lstProductsInDB[16], 0.15],//champignon
            [$lstPlatesInDB[1], $lstProductsInDB[5], 0.05],//salade
            //Crêpe avec dés de filet de poulet
            [$lstPlatesInDB[2], $lstProductsInDB[0], 0.1],//lait
            [$lstPlatesInDB[2], $lstProductsInDB[10], 2],//oeuf
            [$lstPlatesInDB[2], $lstProductsInDB[14], 0.1],//farine de blé
            [$lstPlatesInDB[2], $lstProductsInDB[11], 0.15],//filet de poulet
            [$lstPlatesInDB[2], $lstProductsInDB[5], 0.05],//salade
            //Crêpe au chocolat
            [$lstPlatesInDB[3], $lstProductsInDB[0], 0.1],//lait
            [$lstPlatesInDB[3], $lstProductsInDB[10], 2],//oeuf
            [$lstPlatesInDB[3], $lstProductsInDB[14], 0.1],//farine de blé
            [$lstPlatesInDB[3], $lstProductsInDB[2], 0.05],//filet de poulet
        ];

        foreach ($lstCompose as $value){
            $compose = new Compose();
            $compose->setInPlate($value[0])
                    ->setWithProduct($value[1])
                    ->setQtyPerPlate($value[2]);
            $manager->persist($compose);
            
        }
        $manager->flush();
    }

    /**
     * Method to get a Category for a specific designation
     * @return Category
     */
    private function getCategoryByDesignation(string $designation): Category
    {
        return $this->categoryRepository->findOneBy(array("designation" => $designation));
    }

    /**
     * Method to get a Supplier for a specific name
     * @return Supplier
     */
    private function getSupplierByName(string $name): Supplier
    {
        return $this->supplierRepository->findOneBy(array("name" => $name));
    }

    /**
     * Method to get a Unity for a specific symbol
     * @return Unity
     */
    private function getUnityBySymbol(string $symbol): Unity
    {
        return $this->unityRepository->findOneBy(array("symbol" => $symbol));
    }
}
