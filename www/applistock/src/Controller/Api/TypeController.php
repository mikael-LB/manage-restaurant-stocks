<?php

namespace App\Controller\Api;

use App\Entity\Type;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Type Entity
 * Methods : GET, POST, PUT, DELETE
 */
class TypeController extends AbstractController
{
    /**
     * @Route("/api/types", name="api_type_retrieveall", methods={"GET"})
     */
    public function retrieveAllTypes(TypeRepository $typeRepo): Response
    {
        $types = $typeRepo->findAll();
        return $this->json($types, 200, [], ['groups' => 'group_type']);
    }

    /**
     * @Route("/api/types/{id<\d+>}", name="api_type_retrieve_one", methods={"GET"})
     */
    public function retrieveOneType($id, TypeRepository $typeRepo): Response
    {
        $type = $typeRepo->find($id);
        return $this->json($type, 200, [], ['groups' => 'group_type']);
    }

    /**
     * @Route("/api/types", name="api_type_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createType(
        Request $request,
        TypeRepository $typeRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newType = $serializer->deserialize($json, Type::class, 'json');

        $em->persist($newType);
        $em->flush();
        return $this->json($newType, 200, [], ['groups' => 'group_type']);
    }

    /**
     * @Route("/api/types/{id<\d+>}", name="api_type_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateType(
        $id,
        Request $request,
        TypeRepository $typeRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newType = $serializer->deserialize($json, Type::class, 'json');

        //find the type in db by id in route
        $typeToUpdate = $typeRepo->findOneBy(array("id" => $id));

        //set datas
        $typeToUpdate->setTypename($newType->getTypename());

        //no need to persist, type already exist in db
        $em->flush();
        return $this->json($typeToUpdate, 200, [], ['groups' => 'group_type']);
    }

    /**
     * @Route("/api/types/{id<\d+>}", name="api_type_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteType(
        $id,
        Request $request,
        TypeRepository $typeRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $type = $typeRepo->findOneById($id);
        if (null != $type) {
            $entityManager->remove($type);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
