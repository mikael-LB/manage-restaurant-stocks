<?php

namespace App\Controller\Api;

use App\Entity\Plate;
use App\Repository\PlateRepository;
use App\Service\EntityServiceFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Plate Entity
 * Methods : GET, POST, PUT, DELETE
 */
class PlateController extends AbstractController
{
    /**
     * @Route("/api/plates", name="api_plate_retrieveall", methods={"GET"})
     */
    public function retrieveAllPlates(PlateRepository $plateRepo): Response
    {
        $plates = $plateRepo->findAll();
        return $this->json($plates, 200, [], ['groups' => 'group_plate']);
    }

    /**
     * @Route("/api/plates/{id<\d+>}", name="api_plate_retrieve_one", methods={"GET"})
     */
    public function retrieveOnePlate($id, PlateRepository $plateRepo): Response
    {
        $plate = $plateRepo->find($id);
        return $this->json($plate, 200, [], ['groups' => 'group_plate']);
    }

    /**
     * @Route("/api/plates", name="api_plate_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createPlate(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        EntityServiceFactory $esf
    ): Response {
        $json = $request->getContent();
        $newPlate = $serializer->deserialize($json, Plate::class, 'json');

        // TODO : no id after deserialize so check Category, Supplier, Unity, does a better way exists ?
        // TODO : check values before persist
        $category = $esf->getCategoryService()->getCategoryByDesignation(
            $newPlate->getCategory()->getDesignation()
        );
        $newPlate->setCategory($category);

        $em->persist($newPlate);
        $em->flush();
        return $this->json($newPlate, 200, [], ['groups' => 'group_plate']);
    }

    /**
     * @Route("/api/plates/{id<\d+>}", name="api_plate_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updatePlate(
        $id,
        Request $request,
        PlateRepository $plateRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        EntityServiceFactory $esf
    ): Response {
        // TODO : check values are valid
        $json = $request->getContent();
        //retrieve id of category
        $idCategory = json_decode($json)->category->id;
        //new data to set
        $newPlate = $serializer->deserialize($json, Plate::class, 'json');

        //find the plate in db by id in route
        $plateToUpdate = $plateRepo->findOneBy(array("id" => $id));

        //set datas
        $plateToUpdate->setName($newPlate->getName());
        //retrieve existing objects
        $category = $esf->getCategoryService()->getCategoryById($idCategory);        
        if (null != $category){
            $plateToUpdate->setCategory($category);           
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        //TODO : add the Products with Compose

        //no need to persist, plate already exist in db
        $em->flush();
        return $this->json($plateToUpdate, 200, [], ['groups' => 'group_plate']);
    }

    /**
     * @Route("/api/plates/{id<\d+>}", name="api_plate_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deletePlate(
        $id,
        Request $request,
        PlateRepository $plateRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $plate = $plateRepo->findOneById($id);
        if (null != $plate) {
            $entityManager->remove($plate);
            $entityManager->flush();
        } else {
            return $this->json(['code' => '404', 'message' => 'error'], 404, []);
        }
        return $this->json(['code' => '200', 'message' => 'deleted'], 200, []);
    }
}
