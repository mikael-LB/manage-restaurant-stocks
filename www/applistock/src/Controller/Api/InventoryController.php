<?php

namespace App\Controller\Api;

use App\Entity\Inventory;
use App\Repository\InventoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Inventory Entity
 * Methods : GET, POST, PUT, DELETE
 */
class InventoryController extends AbstractController
{
    /**
     * @Route("/api/inventories", name="api_inventory_retrieveall", methods={"GET"})
     */
    public function retrieveAllInventories(InventoryRepository $inventoryRepo): Response
    {
        $inventories = $inventoryRepo->findAll();
        return $this->json($inventories, 200, [], ['groups' => 'group_inventory']);
    }

    /**
     * @Route("/api/inventories/{id<\d+>}", name="api_inventory_retrieve_one", methods={"GET"})
     */
    public function retrieveOneInventory($id, InventoryRepository $inventoryRepo): Response
    {
        $inventory = $inventoryRepo->find($id);
        return $this->json($inventory, 200, [], ['groups' => 'group_inventory']);
    }

    /**
     * @Route("/api/inventories", name="api_inventory_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createInventory(
        Request $request,
        InventoryRepository $inventoryRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newInventory = $serializer->deserialize($json, Inventory::class, 'json');
        //set IsValidated false
        $newInventory->setIsValidated(false);

        $em->persist($newInventory);
        $em->flush();
        return $this->json($newInventory, 200, [], ['groups' => 'group_inventory']);
    }

    /**
     * @Route("/api/inventories/{id<\d+>}", name="api_inventory_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateInventory(
        $id,
        Request $request,
        InventoryRepository $inventoryRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newInventory = $serializer->deserialize($json, Inventory::class, 'json');

        //find the inventory in db by id in route
        $inventoryToUpdate = $inventoryRepo->findOneBy(array("id" => $id));

        //set datas
        $inventoryToUpdate->setDateInventory($newInventory->getDateInventory());
        $inventoryToUpdate->setIsValidated($newInventory->getIsValidated());

        //no need to persist, inventory already exist in db
        $em->flush();
        return $this->json($inventoryToUpdate, 200, [], ['groups' => 'group_inventory']);
    }

    /**
     * @Route("/api/inventories/{id<\d+>}", name="api_inventory_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteInventory(
        $id,
        Request $request,
        InventoryRepository $inventoryRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Quantity...
        $inventory = $inventoryRepo->findOneById($id);
        if (null != $inventory) {
            $entityManager->remove($inventory);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
