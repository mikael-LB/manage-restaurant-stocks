<?php

namespace App\Controller\Api;

use App\Entity\JobTitle;
use App\Repository\JobTitleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on JobTitle Entity
 * Methods : GET, POST, PUT, DELETE
 */
class JobTitleController extends AbstractController
{
    /**
     * @Route("/api/jobtitles", name="api_jobtitle_retrieveall", methods={"GET"})
     */
    public function retrieveAllJobTitles(JobTitleRepository $jobtitleRepo): Response
    {
        $jobtitles = $jobtitleRepo->findAll();
        return $this->json($jobtitles, 200, [], ['groups' => 'group_jobtitle']);
    }

    /**
     * @Route("/api/jobtitles/{id<\d+>}", name="api_jobtitle_retrieve_one", methods={"GET"})
     */
    public function retrieveOneJobTitle($id, JobTitleRepository $jobtitleRepo): Response
    {
        $jobtitle = $jobtitleRepo->find($id);
        return $this->json($jobtitle, 200, [], ['groups' => 'group_jobtitle']);
    }

    /**
     * @Route("/api/jobtitles", name="api_jobtitle_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createJobTitle(
        Request $request,
        JobTitleRepository $jobtitleRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newJobTitle = $serializer->deserialize($json, JobTitle::class, 'json');

        $em->persist($newJobTitle);
        $em->flush();
        return $this->json($newJobTitle, 200, [], ['groups' => 'group_jobtitle']);
    }

    /**
     * @Route("/api/jobtitles/{id<\d+>}", name="api_jobtitle_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateJobTitle(
        $id,
        Request $request,
        JobTitleRepository $jobtitleRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newJobTitle = $serializer->deserialize($json, JobTitle::class, 'json');

        //find the jobtitle in db by id in route
        $jobtitleToUpdate = $jobtitleRepo->findOneBy(array("id" => $id));

        //set datas
        $jobtitleToUpdate->setName($newJobTitle->getName());

        //no need to persist, jobtitle already exist in db
        $em->flush();
        return $this->json($jobtitleToUpdate, 200, [], ['groups' => 'group_jobtitle']);
    }

    /**
     * @Route("/api/jobtitles/{id<\d+>}", name="api_jobtitle_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteJobTitle(
        $id,
        Request $request,
        JobTitleRepository $jobtitleRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $jobtitle = $jobtitleRepo->findOneById($id);
        if (null != $jobtitle) {
            $entityManager->remove($jobtitle);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
