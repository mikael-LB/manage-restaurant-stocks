<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use App\Service\EntityServiceFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Product Entity
 * Methods : GET, POST, PUT, DELETE
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/api/products", name="api_product_retrieveall", methods={"GET"})
     */
    public function retrieveAllProducts(ProductRepository $productRepo): Response
    {
        $products = $productRepo->findAll();

        return $this->json($products, 200, [], ['groups' => 'group_product']);
    }

    /**
     * @Route("/api/products/{id<\d+>}", name="api_product_retrieve_one", methods={"GET"})
     */
    public function retrieveOneProduct($id, ProductRepository $productRepo): Response
    {
        $product = $productRepo->find($id);
        return $this->json($product, 200, [], ['groups' => 'group_product']);
    }

    /**
     * @Route("/api/products", name="api_product_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createProduct(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        EntityServiceFactory $esf
    ): Response {
        $json = $request->getContent();
        $product = $serializer->deserialize($json, Product::class, 'json');

        // TODO : no id after deserialize so check Category, Supplier, Unity, does a better way exists ?
        // TODO : check values before persist
        $category = $esf->getCategoryService()->getCategoryByDesignation(
            $product->getCategory()->getDesignation()
        );
        $product->setCategory($category);

        $supplier = $esf->getSupplierService()->getSupplierByName(
            $product->getSupplier()->getName()
        );
        $product->setSupplier($supplier);

        $unity = $esf->getUnityService()->getUnityBySymbol(
            $product->getUnity()->getSymbol()
        );
        $product->setUnity($unity);

        $entityManager->persist($product);
        $entityManager->flush();
        return $this->json($product, 201, [], ['groups' => 'group_product']);
    }

    /**
     * @Route("/api/products/{id<\d+>}", name="api_product_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateProduct(
        $id,
        Request $request,
        SerializerInterface $serializer,
        ProductRepository $productRepo,
        EntityManagerInterface $entityManager,
        EntityServiceFactory $esf
    ): Response {
        // TODO : check values are valid
        $json = $request->getContent();
        //retrieve the ids from json for Category, Supplier, Unity
        $ids = $esf->getProductService()->retrieveIds($json);
        //new data to set
        $newProduct = $serializer->deserialize($json, Product::class, 'json');

        //find the product in db by id in route
        $productToUpdate = $productRepo->findOneBy(array("id" => $id));

        //set datas
        $productToUpdate->setDesignation($newProduct->getDesignation());
        $productToUpdate->setQtyInStck($newProduct->getQtyInStck());
        $productToUpdate->setPrice($newProduct->getPrice());

        //retrieve existing objects
        $category = $esf->getCategoryService()->getCategoryById($ids["category"]);
        $supplier = $esf->getSupplierService()->getSupplierById($ids["supplier"]);
        $unity = $esf->getUnityService()->getUnityById($ids["unity"]);

        if (null != $category && null != $supplier && null != $unity){
            $productToUpdate->setCategory($category);
            $productToUpdate->setSupplier($supplier);
            $productToUpdate->setUnity($unity);
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }

        //no need to persist, product already exist in db
        $entityManager->flush();
        return $this->json($productToUpdate, 200, [], ['groups' => 'group_product']);
    }

    /**
     * @Route("/api/products/{id<\d+>}", name="api_product_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteProduct(
        $id,
        Request $request,
        ProductRepository $productRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        $product = $productRepo->findOneById($id);
        if (null != $product) {
            $entityManager->remove($product);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }

    /**
     * @Route("/api/products/category/{id<\d+>}", name="api_product_retrieve_one_for_category", methods={"GET"})
     */
    public function retrieveOneProductForGivenCategory($id, ProductRepository $productRepo, CategoryRepository $categoryRepo): Response
    {
        $category = $categoryRepo->findOneById($id);
        $products = $productRepo->findProductsForGivenCategory($category);
        return $this->json($products, 200, [], ['groups' => 'group_product']);
    }
}
