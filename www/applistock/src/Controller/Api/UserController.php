<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\EntityServiceFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class which define some routes to make requests on User Entity
 * Methods : GET, POST, PUT, DELETE
 */
class UserController extends AbstractController
{
    /**
     * @Route("/api/users", name="api_user_retrieveall", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function retrieveAllUsers(UserRepository $userRepo): Response
    {
        $users = $userRepo->findAll();
        return $this->json($users, 200, [], ['groups' => 'group_user']);
    }

    /**
     * @Route("/api/users/{id<\d+>}", name="api_user_retrieve_one", methods={"GET"})
     */
    public function retrieveOneUser($id, UserRepository $userRepo): Response
    {
        //TODO : check if current user and the route id are equals if the user is not an admin
        $user = $userRepo->find($id);
        return $this->json($user, 200, [], ['groups' => 'group_user']);
    }

    /**
     * @Route("/api/users/user", name="api_user_retrieve_user", methods={"GET"})
     */
    public function retrieveUser(UserRepository $userRepo):Response
    {
        $user = $this->getUser();
        return $this->json($user, 200, [], ['groups' => 'group_user']);
    }

    /**
     * @Route("/api/users", name="api_user_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createUser(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityServiceFactory $esf
    ): Response {
        $json = $request->getContent();
        //retrieve id for the JobTitle
        $idJobTitle = json_decode($json)->jobtitle->id;
        //new Datas to set        
        //TODO : unique email and valid email format
        $newUser = $serializer->deserialize($json, User::class, 'json');
        //no id for Jobtitle after deserialization so retrieve JobTitle
        $jobTitle = $esf->getJobTitleService()->getJobTitleById($idJobTitle);
        $newUser->setJobtitle($jobTitle);
        //TODO : photoUrl
        //TODO : check same password and password confirm
        $newUser->setPassword($passwordEncoder->encodePassword($newUser,$newUser->getPassword()));

        $em->persist($newUser);
        $em->flush();
        return $this->json($newUser, 200, [], ['groups' => 'group_user']);
    }

    /**
     * @Route("/api/users/{id<\d+>}", name="api_user_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateUser(
        $id,
        Request $request,
        UserRepository $userRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityServiceFactory $esf
    ): Response {
        $json = $request->getContent();
        //retrieve id for the JobTitle
        $idJobTitle = json_decode($json)->jobtitle->id;
        //new Datas to set
        $newUser = $serializer->deserialize($json, User::class, 'json');

        //find the user in db by id in route
        $userToUpdate = $userRepo->findOneBy(array("id" => $id));

        //set datas
        //TODO : unique email and valid email format
        $userToUpdate->setEmail($newUser->getEmail())
            ->setRoles($newUser->getRoles())
            ->setLastname($newUser->getLastname())
            ->setFirstname($newUser->getFirstname())
            ->setIsActivated($newUser->getIsActivated());
        $jobTitle = $esf->getJobTitleService()->getJobTitleById($idJobTitle);
        $userToUpdate->setJobtitle($jobTitle);
        //TODO : photoUrl
        //TODO : check same password and password confirm
        $userToUpdate->setPassword($passwordEncoder->encodePassword($userToUpdate,$newUser->getPassword()));

        //no need to persist, user already exist in db
        $em->flush();
        return $this->json($userToUpdate, 200, [], ['groups' => 'group_user']);
    }

    /**
     * @Route("/api/users/{id<\d+>}", name="api_user_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteUser(
        $id,
        Request $request,
        UserRepository $userRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $user = $userRepo->findOneById($id);
        if (null != $user) {
            $entityManager->remove($user);
            $entityManager->flush();
        } else {
            return $this->json(['code' => '404', 'message' => 'error'], 404, []);
        }
        return $this->json(['code' => '200', 'message' => 'deleted'], 200, []);
    }
}
