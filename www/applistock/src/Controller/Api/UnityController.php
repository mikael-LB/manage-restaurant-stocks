<?php

namespace App\Controller\Api;

use App\Entity\Unity;
use App\Repository\UnityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Unity Entity
 * Methods : GET, POST, PUT, DELETE
 */
class UnityController extends AbstractController
{
    /**
     * @Route("/api/unities", name="api_unity_retrieveall", methods={"GET"})
     */
    public function retrieveAllUnities(UnityRepository $unityRepo): Response
    {
        $unities = $unityRepo->findAll();
        return $this->json($unities, 200, [], ['groups' => 'group_unity']);
    }

    /**
     * @Route("/api/unities/{id<\d+>}", name="api_unity_retrieve_one", methods={"GET"})
     */
    public function retrieveOneUnity($id, UnityRepository $unityRepo): Response
    {
        $unity = $unityRepo->find($id);
        return $this->json($unity, 200, [], ['groups' => 'group_unity']);
    }

    /**
     * @Route("/api/unities", name="api_unity_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createUnity(
        Request $request,
        UnityRepository $unityRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newUnity = $serializer->deserialize($json, Unity::class, 'json');

        $em->persist($newUnity);
        $em->flush();
        return $this->json($newUnity, 200, [], ['groups' => 'group_unity']);
    }

    /**
     * @Route("/api/unities/{id<\d+>}", name="api_unity_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateUnity(
        $id,
        Request $request,
        UnityRepository $unityRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newUnity = $serializer->deserialize($json, Unity::class, 'json');

        //find the unity in db by id in route
        $unityToUpdate = $unityRepo->findOneBy(array("id" => $id));

        //set datas
        $unityToUpdate->setDesignation($newUnity->getDesignation());
        $unityToUpdate->setSymbol($newUnity->getSymbol());

        //no need to persist, unity already exist in db
        $em->flush();
        return $this->json($unityToUpdate, 200, [], ['groups' => 'group_unity']);
    }

    /**
     * @Route("/api/unities/{id<\d+>}", name="api_unity_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteUnity(
        $id,
        Request $request,
        UnityRepository $unityRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $unity = $unityRepo->findOneById($id);
        if (null != $unity) {
            $entityManager->remove($unity);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
