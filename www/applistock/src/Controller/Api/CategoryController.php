<?php

namespace App\Controller\Api;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Category Entity
 * Methods : GET, POST, PUT, DELETE
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/api/categories", name="api_category_retrieveall", methods={"GET"})
     */
    public function retrieveAllCategories(CategoryRepository $categoryRepo): Response
    {
        $categories = $categoryRepo->findAll();
        return $this->json($categories, 200, [], ['groups' => 'group_category']);
    }

    /**
     * @Route("/api/categories/{id<\d+>}", name="api_category_retrieve_one", methods={"GET"})
     */
    public function retrieveOneCategory($id, CategoryRepository $categoryRepo): Response
    {
        $category = $categoryRepo->find($id);
        return $this->json($category, 200, [], ['groups' => 'group_category']);
    }

    /**
     * @Route("/api/categories", name="api_category_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createCategory(
        Request $request,
        CategoryRepository $categoryRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newCategory = $serializer->deserialize($json, Category::class, 'json');

        $em->persist($newCategory);
        $em->flush();
        return $this->json($newCategory, 200, [], ['groups' => 'group_category']);
    }

    /**
     * @Route("/api/categories/{id<\d+>}", name="api_category_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateCategory(
        $id,
        Request $request,
        CategoryRepository $categoryRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newCategory = $serializer->deserialize($json, Category::class, 'json');

        //find the category in db by id in route
        $categoryToUpdate = $categoryRepo->findOneBy(array("id" => $id));

        if ($categoryToUpdate !== null){
            //set datas
            $categoryToUpdate->setDesignation($newCategory->getDesignation());
        } else {
            return $this->json(['code'=>'404','message' => 'not found'], 404, []);
        }
        //no need to persist, category already exist in db
        $em->flush();
        return $this->json($categoryToUpdate, 200, [], ['groups' => 'group_category']);
    }

    /**
     * @Route("/api/categories/{id<\d+>}", name="api_category_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteCategory(
        $id,
        Request $request,
        CategoryRepository $categoryRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $category = $categoryRepo->findOneById($id);
        if (null != $category) {
            $entityManager->remove($category);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
