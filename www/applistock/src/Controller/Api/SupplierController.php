<?php

namespace App\Controller\Api;

use App\Entity\Supplier;
use App\Repository\SupplierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class which define some routes to make requests on Supplier Entity
 * Methods : GET, POST, PUT, DELETE
 */
class SupplierController extends AbstractController
{
    /**
     * @Route("/api/suppliers", name="api_supplier_retrieveall", methods={"GET"})
     */
    public function retrieveAllSuppliers(SupplierRepository $supplierRepo): Response
    {
        $suppliers = $supplierRepo->findAll();
        return $this->json($suppliers, 200, [], ['groups' => 'group_supplier']);
    }

    /**
     * @Route("/api/suppliers/{id<\d+>}", name="api_supplier_retrieve_one", methods={"GET"})
     */
    public function retrieveOneSupplier($id, SupplierRepository $supplierRepo): Response
    {
        $supplier = $supplierRepo->find($id);
        return $this->json($supplier, 200, [], ['groups' => 'group_supplier']);
    }

    /**
     * @Route("/api/suppliers", name="api_supplier_create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function createSupplier(
        Request $request,
        SupplierRepository $supplierRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newSupplier = $serializer->deserialize($json, Supplier::class, 'json');

        $em->persist($newSupplier);
        $em->flush();
        return $this->json($newSupplier, 200, [], ['groups' => 'group_supplier']);
    }

    /**
     * @Route("/api/suppliers/{id<\d+>}", name="api_supplier_update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function updateSupplier(
        $id,
        Request $request,
        SupplierRepository $supplierRepo,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ): Response {
        $json = $request->getContent();
        $newSupplier = $serializer->deserialize($json, Supplier::class, 'json');

        //find the supplier in db by id in route
        $supplierToUpdate = $supplierRepo->findOneBy(array("id" => $id));

        //set datas
        $supplierToUpdate->setName($newSupplier->getName());
        $supplierToUpdate->setPhone($newSupplier->getPhone());
        $supplierToUpdate->setActivity($newSupplier->getActivity());

        //no need to persist, supplier already exist in db
        $em->flush();
        return $this->json($supplierToUpdate, 200, [], ['groups' => 'group_supplier']);
    }

    /**
     * @Route("/api/suppliers/{id<\d+>}", name="api_supplier_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteSupplier(
        $id,
        Request $request,
        SupplierRepository $supplierRepo,
        EntityManagerInterface $entityManager
    ): Response {
        $json = $request->getContent();

        //TODO : check if there is an associated Product...
        $supplier = $supplierRepo->findOneById($id);
        if (null != $supplier) {
            $entityManager->remove($supplier);
            $entityManager->flush();
        } else {
            return $this->json(['code'=>'404','message' => 'error'], 404, []);
        }
        return $this->json(['code'=>'200','message' => 'deleted'], 200, []);
    }
}
