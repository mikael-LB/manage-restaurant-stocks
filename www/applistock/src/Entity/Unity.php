<?php

namespace App\Entity;

use App\Repository\UnityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UnityRepository::class)
 * @ORM\Table(name="`app_unity`")
 */
class Unity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group_product","group_unity","group_plate"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @Groups({"group_product","group_unity"})
     */
    private $designation;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"group_product","group_unity","group_plate"})
     */
    private $symbol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }
}
