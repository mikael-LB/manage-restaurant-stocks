<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="`app_product`")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group_product","group_category","group_plate"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"group_product","group_plate"})
     */
    private $designation;

    /**
     * @ORM\Column(type="float")
     * @Groups({"group_product"})
     */
    private $qtyInStck;

    /**
     * @ORM\Column(type="float")
     * @Groups({"group_product"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_product","group_plate"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_product","group_plate"})
     */
    private $supplier;

    /**
     * @ORM\ManyToOne(targetEntity=Unity::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_product","group_plate"})
     */
    private $unity;

    /**
     * @ORM\OneToMany(targetEntity=Compose::class, mappedBy="withProduct", orphanRemoval=true)
     * @Groups({"group_product"})
     */
    private $composes;

    /**
     * @ORM\OneToMany(targetEntity=Quantity::class, mappedBy="ofProduct")
     */
    private $quantities;

    public function __construct()
    {
        $this->inventory = new ArrayCollection();
        $this->composes = new ArrayCollection();
        $this->quantities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getQtyInStck(): ?float
    {
        return $this->qtyInStck;
    }

    public function setQtyInStck(float $qtyInStck): self
    {
        $this->qtyInStck = $qtyInStck;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getUnity(): ?Unity
    {
        return $this->unity;
    }

    public function setUnity(?Unity $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    /**
     * @return Collection|Compose[]
     */
    public function getComposes(): Collection
    {
        return $this->composes;
    }

    public function addCompose(Compose $compose): self
    {
        if (!$this->composes->contains($compose)) {
            $this->composes[] = $compose;
            $compose->setWithProduct($this);
        }

        return $this;
    }

    public function removeCompose(Compose $compose): self
    {
        if ($this->composes->removeElement($compose)) {
            // set the owning side to null (unless already changed)
            if ($compose->getWithProduct() === $this) {
                $compose->setWithProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quantity[]
     */
    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    public function addQuantity(Quantity $quantity): self
    {
        if (!$this->quantities->contains($quantity)) {
            $this->quantities[] = $quantity;
            $quantity->setOfProduct($this);
        }

        return $this;
    }

    public function removeQuantity(Quantity $quantity): self
    {
        if ($this->quantities->removeElement($quantity)) {
            // set the owning side to null (unless already changed)
            if ($quantity->getOfProduct() === $this) {
                $quantity->setOfProduct(null);
            }
        }

        return $this;
    }
}
