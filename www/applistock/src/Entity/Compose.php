<?php

namespace App\Entity;

use App\Repository\ComposeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ComposeRepository::class)
 * @ORM\Table(name="`app_compose`")
 */
class Compose
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group_product","group_plate"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"group_plate"})
     */
    private $qtyPerPlate;

    /**
     * @ORM\ManyToOne(targetEntity=Plate::class, inversedBy="composes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_product"})
     */
    private $inPlate;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="composes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_plate"})
     */
    private $withProduct;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQtyPerPlate(): ?float
    {
        return $this->qtyPerPlate;
    }

    public function setQtyPerPlate(float $qtyPerPlate): self
    {
        $this->qtyPerPlate = $qtyPerPlate;

        return $this;
    }

    public function getInPlate(): ?Plate
    {
        return $this->inPlate;
    }

    public function setInPlate(?Plate $inPlate): self
    {
        $this->inPlate = $inPlate;

        return $this;
    }

    public function getWithProduct(): ?Product
    {
        return $this->withProduct;
    }

    public function setWithProduct(?Product $withProduct): self
    {
        $this->withProduct = $withProduct;

        return $this;
    }
}
