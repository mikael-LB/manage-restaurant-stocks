<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 * @ORM\Table(name="`app_inventory`")
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group_inventory"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"group_inventory"})
     */
    private $dateInventory;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"group_inventory"})
     */
    private $isValidated;

    /**
     * @ORM\OneToMany(targetEntity=Quantity::class, mappedBy="forInventory", orphanRemoval=true)
     */
    private $quantities;

    public function __construct()
    {
        $this->quantities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateInventory(): ?\DateTimeInterface
    {
        return $this->dateInventory;
    }

    public function setDateInventory(\DateTimeInterface $dateInventory): self
    {
        $this->dateInventory = $dateInventory;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * @return Collection|Quantity[]
     */
    public function getQuantities(): Collection
    {
        return $this->quantities;
    }

    public function addQuantity(Quantity $quantity): self
    {
        if (!$this->quantities->contains($quantity)) {
            $this->quantities[] = $quantity;
            $quantity->setForInventory($this);
        }

        return $this;
    }

    public function removeQuantity(Quantity $quantity): self
    {
        if ($this->quantities->removeElement($quantity)) {
            // set the owning side to null (unless already changed)
            if ($quantity->getForInventory() === $this) {
                $quantity->setForInventory(null);
            }
        }

        return $this;
    }
}
