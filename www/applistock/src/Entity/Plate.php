<?php

namespace App\Entity;

use App\Repository\PlateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PlateRepository::class)
 * @ORM\Table(name="`app_plate`")
 */
class Plate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group_category","group_plate"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Groups({"group_plate"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="plates")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"group_plate"})
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=Compose::class, mappedBy="inPlate", orphanRemoval=true)
     * @Groups({"group_plate"})
     */
    private $composes;

    public function __construct()
    {
        $this->composes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Compose[]
     */
    public function getComposes(): Collection
    {
        return $this->composes;
    }

    public function addCompose(Compose $compose): self
    {
        if (!$this->composes->contains($compose)) {
            $this->composes[] = $compose;
            $compose->setInPlate($this);
        }

        return $this;
    }

    public function removeCompose(Compose $compose): self
    {
        if ($this->composes->removeElement($compose)) {
            // set the owning side to null (unless already changed)
            if ($compose->getInPlate() === $this) {
                $compose->setInPlate(null);
            }
        }

        return $this;
    }
}
