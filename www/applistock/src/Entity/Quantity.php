<?php

namespace App\Entity;

use App\Repository\QuantityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuantityRepository::class)
 * @ORM\Table(name="`app_quantity`")
 */
class Quantity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalPerUnit;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="quantities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ofProduct;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="quantities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $forInventory;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalPerUnit(): ?float
    {
        return $this->totalPerUnit;
    }

    public function setTotalPerUnit(?float $totalPerUnit): self
    {
        $this->totalPerUnit = $totalPerUnit;

        return $this;
    }

    public function getOfProduct(): ?Product
    {
        return $this->ofProduct;
    }

    public function setOfProduct(?Product $ofProduct): self
    {
        $this->ofProduct = $ofProduct;

        return $this;
    }

    public function getForInventory(): ?Inventory
    {
        return $this->forInventory;
    }

    public function setForInventory(?Inventory $forInventory): self
    {
        $this->forInventory = $forInventory;

        return $this;
    }
}
