<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126083511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `app_category` (id INT AUTO_INCREMENT NOT NULL, designation VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_compose` (id INT AUTO_INCREMENT NOT NULL, in_plate_id INT NOT NULL, with_product_id INT NOT NULL, qty_per_plate DOUBLE PRECISION NOT NULL, INDEX IDX_4311945B989F95D7 (in_plate_id), INDEX IDX_4311945B715E8D12 (with_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_inventory` (id INT AUTO_INCREMENT NOT NULL, date_inventory DATETIME NOT NULL, is_validated TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_jobtitle` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_plate` (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(40) NOT NULL, INDEX IDX_A74D5A9612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_product` (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, supplier_id INT NOT NULL, unity_id INT NOT NULL, designation VARCHAR(40) NOT NULL, qty_in_stck DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_3E1784E012469DE2 (category_id), INDEX IDX_3E1784E02ADD6D8C (supplier_id), INDEX IDX_3E1784E0F6859C8C (unity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_quantity` (id INT AUTO_INCREMENT NOT NULL, of_product_id INT NOT NULL, for_inventory_id INT NOT NULL, total_per_unit DOUBLE PRECISION DEFAULT NULL, INDEX IDX_9773769BACD0A7CF (of_product_id), INDEX IDX_9773769BC71DDDD2 (for_inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_supplier` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, phone VARCHAR(12) NOT NULL, activity VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_type` (id INT AUTO_INCREMENT NOT NULL, typename VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_unity` (id INT AUTO_INCREMENT NOT NULL, designation VARCHAR(30) NOT NULL, symbol VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `app_user` (id INT AUTO_INCREMENT NOT NULL, jobtitle_id INT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, lastname VARCHAR(30) NOT NULL, firstname VARCHAR(30) NOT NULL, photo_url VARCHAR(255) DEFAULT NULL, is_activated TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_88BDF3E9E7927C74 (email), INDEX IDX_88BDF3E9E438D15B (jobtitle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `app_compose` ADD CONSTRAINT FK_4311945B989F95D7 FOREIGN KEY (in_plate_id) REFERENCES `app_plate` (id)');
        $this->addSql('ALTER TABLE `app_compose` ADD CONSTRAINT FK_4311945B715E8D12 FOREIGN KEY (with_product_id) REFERENCES `app_product` (id)');
        $this->addSql('ALTER TABLE `app_plate` ADD CONSTRAINT FK_A74D5A9612469DE2 FOREIGN KEY (category_id) REFERENCES `app_category` (id)');
        $this->addSql('ALTER TABLE `app_product` ADD CONSTRAINT FK_3E1784E012469DE2 FOREIGN KEY (category_id) REFERENCES `app_category` (id)');
        $this->addSql('ALTER TABLE `app_product` ADD CONSTRAINT FK_3E1784E02ADD6D8C FOREIGN KEY (supplier_id) REFERENCES `app_supplier` (id)');
        $this->addSql('ALTER TABLE `app_product` ADD CONSTRAINT FK_3E1784E0F6859C8C FOREIGN KEY (unity_id) REFERENCES `app_unity` (id)');
        $this->addSql('ALTER TABLE `app_quantity` ADD CONSTRAINT FK_9773769BACD0A7CF FOREIGN KEY (of_product_id) REFERENCES `app_product` (id)');
        $this->addSql('ALTER TABLE `app_quantity` ADD CONSTRAINT FK_9773769BC71DDDD2 FOREIGN KEY (for_inventory_id) REFERENCES `app_inventory` (id)');
        $this->addSql('ALTER TABLE `app_user` ADD CONSTRAINT FK_88BDF3E9E438D15B FOREIGN KEY (jobtitle_id) REFERENCES `app_jobtitle` (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `app_plate` DROP FOREIGN KEY FK_A74D5A9612469DE2');
        $this->addSql('ALTER TABLE `app_product` DROP FOREIGN KEY FK_3E1784E012469DE2');
        $this->addSql('ALTER TABLE `app_quantity` DROP FOREIGN KEY FK_9773769BC71DDDD2');
        $this->addSql('ALTER TABLE `app_user` DROP FOREIGN KEY FK_88BDF3E9E438D15B');
        $this->addSql('ALTER TABLE `app_compose` DROP FOREIGN KEY FK_4311945B989F95D7');
        $this->addSql('ALTER TABLE `app_compose` DROP FOREIGN KEY FK_4311945B715E8D12');
        $this->addSql('ALTER TABLE `app_quantity` DROP FOREIGN KEY FK_9773769BACD0A7CF');
        $this->addSql('ALTER TABLE `app_product` DROP FOREIGN KEY FK_3E1784E02ADD6D8C');
        $this->addSql('ALTER TABLE `app_product` DROP FOREIGN KEY FK_3E1784E0F6859C8C');
        $this->addSql('DROP TABLE `app_category`');
        $this->addSql('DROP TABLE `app_compose`');
        $this->addSql('DROP TABLE `app_inventory`');
        $this->addSql('DROP TABLE `app_jobtitle`');
        $this->addSql('DROP TABLE `app_plate`');
        $this->addSql('DROP TABLE `app_product`');
        $this->addSql('DROP TABLE `app_quantity`');
        $this->addSql('DROP TABLE `app_supplier`');
        $this->addSql('DROP TABLE `app_type`');
        $this->addSql('DROP TABLE `app_unity`');
        $this->addSql('DROP TABLE `app_user`');
    }
}
